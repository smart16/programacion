<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Add custom values by settings them to the $config array.
// Example: $config['smtp_host'] = 'smtp.gmail.com'; 
// @link https://codeigniter.com/user_guide/libraries/email.html

$config['useragent'] = 'Easy!Appointments';
$config['protocol'] = 'mail'; // or 'smtp'
$config['mailtype'] = 'html'; // or 'text'
$config['smtp_host'] = 'mail.smart.edu.co';
$config['smtp_user'] = 'programacion@smart.edu.co'; 
$config['smtp_pass'] = 'Smart2020';
$config['smtp_crypto'] = 'ssl'; // or 'tls'
$config['smtp_port'] = 25;

// Configuración cabeceras del mensaje
$mail->From = "programacion@smart.edu.co";
$mail->FromName = "Smart";